# Corso dottorato 2022

## Tecniche avanzate di analisi dati

### Lezione 1 - Stimatori all'opera

[Trasparenze](slides/DataAnalysis1.pdf)

[Notebook (python)](NeutrinoSpeed.ipynb) 


### Lezione 2 - Questione di confidenza

[Trasparenze](slides/DataAnalysis2.pdf)

[Notebook (ROOT)](Efficiency.ipynb) 

### Lezione 3 - Una scoperta verosimile

[Trasparenze](slides/DataAnalysis3.pdf)

[Notebook (RooFit)](BsSpectroscopy.ipynb) 

### Lezione 4 - Inferenza pandemica

[Trasparenze](slides/DataAnalysis4.pdf)

[Notebook 1 (R)](MCMC.ipynb) 

[Notebook 2 (R)](Rt.ipynb) 

### Lezione 5 - Decisioni difficili 1

[Trasparenze](slides/DataAnalysis5.pdf)

[Notebook (R)](Iris.ipynb) 

### Lezione 6 - Decisioni difficili 2

[Trasparenze](slides/DataAnalysis6.pdf)

[Notebook (scikit-learn/tensorflow)](Tau3mu.ipynb) 
